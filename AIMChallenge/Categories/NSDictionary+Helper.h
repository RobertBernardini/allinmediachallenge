//
//  NSDictionary+Helper.h
//  AIMChallenge
//
//  Created by Robert Bernardini on 9/15/18.
//  Copyright © 2018 Robert Bernardini. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDictionary (Helper)

// Checks if the value for a key is NULL.
- (id)notNullForKey:(NSString *)key;

@end
