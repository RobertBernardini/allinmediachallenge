//
//  NSError+Helper.m
//  AIMChallenge
//
//  Created by Robert Bernardini on 9/17/18.
//  Copyright © 2018 Robert Bernardini. All rights reserved.
//

#import "NSError+Helper.h"

@implementation NSError (Helper)

+ (instancetype)errorWithCode:(NSInteger)code message:(NSString *)message {
    NSDictionary *userInfo = @{NSLocalizedDescriptionKey: message,
                               NSLocalizedFailureReasonErrorKey: message,
                               NSLocalizedRecoverySuggestionErrorKey: message};
    return [NSError errorWithDomain:@"" code:code userInfo:userInfo];
}

@end
