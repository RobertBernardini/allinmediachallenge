//
//  NSDate+Helper.m
//  AIMChallenge
//
//  Created by Robert Bernardini on 9/18/18.
//  Copyright © 2018 Robert Bernardini. All rights reserved.
//

#import "NSDate+Helper.h"

@implementation NSDate (Helper)

- (NSString *)toString {
    NSDateFormatter *dateFormatter = [NSDateFormatter new];
    [dateFormatter setDateFormat:@"h:mm:ss a, E, d MMM"];
    return [dateFormatter stringFromDate:self];
}

- (NSString *)toDurationString {
    NSDateFormatter *dateFormatter = [NSDateFormatter new];
    [dateFormatter setDateFormat:@"mm:ss"];
    return [dateFormatter stringFromDate:self];
}

+ (NSDate *)dateFromString:(NSString *)date {
    NSDateFormatter *dateFormatter = [NSDateFormatter new];
    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ssZ"];
    return [dateFormatter dateFromString:date];
}

+ (NSDate *)durationDateFromString:(NSString *)duration {
    NSDateFormatter *dateFormatter = [NSDateFormatter new];
    [dateFormatter setDateFormat:@"HH:mm:ss"];
    return [dateFormatter dateFromString:duration];
}

@end
