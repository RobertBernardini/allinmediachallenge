//
//  NSDate+Helper.h
//  AIMChallenge
//
//  Created by Robert Bernardini on 9/18/18.
//  Copyright © 2018 Robert Bernardini. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDate (Helper)

// Converts the date object to a string in the following format: h:mm:ss a, E, d MMM.
- (NSString *)toString;

// Converts the date object (containing only time) to a duration string in the following format: mm:ss.
- (NSString *)toDurationString;

// Returns a date object from a date string with the following format: yyyy-MM-dd'T'HH:mm:ssZ.
+ (NSDate *)dateFromString:(NSString *)date;

// Returns a time only date object from a date string with the following format: HH:mm:ss.
+ (NSDate *)durationDateFromString:(NSString *)duration;

@end
