//
//  NSDictionary+Helper.m
//  AIMChallenge
//
//  Created by Robert Bernardini on 9/15/18.
//  Copyright © 2018 Robert Bernardini. All rights reserved.
//

#import "NSDictionary+Helper.h"

@implementation NSDictionary (Helper)

- (id)notNullForKey:(NSString *)key {
    id value = self[key];
    if (value == [NSNull null]) {
        return nil;
    }
    return value;
}

@end
