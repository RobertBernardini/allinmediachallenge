//
//  NSError+Helper.h
//  AIMChallenge
//
//  Created by Robert Bernardini on 9/17/18.
//  Copyright © 2018 Robert Bernardini. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSError (Helper)

// Instantiates an error with the given code and message.
+ (instancetype)errorWithCode:(NSInteger)code message:(NSString *)message;

@end
