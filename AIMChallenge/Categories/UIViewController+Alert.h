//
//  UIViewController+Alert.h
//  AIMChallenge
//
//  Created by Robert Bernardini on 9/17/18.
//  Copyright © 2018 Robert Bernardini. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIViewController (Alert)

// Shows an alert message popup. The completion is called after the alert has been presented.
- (void)showAlertWithTitle:(NSString *)title message:(NSString *)message completion:(void (^)(void))completion;

@end
