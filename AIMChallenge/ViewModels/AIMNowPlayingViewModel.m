//
//  AIMNowPlayingViewModel.m
//  AIMChallenge
//
//  Created by Robert Bernardini on 9/18/18.
//  Copyright © 2018 Robert Bernardini. All rights reserved.
//

@import UIKit;
#import "AIMNowPlayingViewModel.h"
#import "AIMPlayoutItem.h"
#import "AIMClient.h"
#import "NSError+Helper.h"
#import "NSDate+Helper.h"

@implementation AIMNowPlayingViewModel

#pragma mark - Lifecycle

- (instancetype)initWithPlayoutItem:(AIMPlayoutItem *)item {
    self = [super init];
    if (self) {
        self.artist = item.artist;
        self.duration = item.duration.toDurationString;
        self.imageURL = [NSURL URLWithString:item.imageURL];
        self.previewURL = [NSURL URLWithString:item.previewURL];
        self.status = [item.status isEqualToString:@"playing"] ? @"Currently Playing" : [NSString stringWithFormat:@"Played at %@", item.time.toString];
        self.statusColor = [item.status isEqualToString:@"playing"] ? [UIColor greenColor] : [UIColor redColor];
        self.title = item.title;
        self.type = item.type;
        self.iTunesTitle = item.iTunesTitle;
        self.iTunesURL = [NSURL URLWithString:item.iTunesURL];
    }
    return self;
}

#pragma mark - Public

+ (void)getNowPlayingViewModelsWithSuccess:(void (^)(NSArray<AIMNowPlayingViewModel *> *viewModels))success failure:(void (^)(NSError *error))failure {
    NSMutableArray *viewModels = [NSMutableArray new];
    AIMClient *client = [AIMClient sharedClient];
    [client getNowPlayingFromNovaWithSuccess:^(NSArray *playoutItems) {
        for (AIMPlayoutItem *item in playoutItems) {
            AIMNowPlayingViewModel *viewModel = [[self alloc] initWithPlayoutItem:item];
            [viewModels addObject:viewModel];
        }
        success([viewModels copy]);
        
    } failure:failure];
}

@end
