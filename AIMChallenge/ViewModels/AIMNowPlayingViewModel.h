//
//  AIMNowPlayingViewModel.h
//  AIMChallenge
//
//  Created by Robert Bernardini on 9/18/18.
//  Copyright © 2018 Robert Bernardini. All rights reserved.
//

#import <Foundation/Foundation.h>
@class AIMPlayoutItem;

@interface AIMNowPlayingViewModel : NSObject

@property (strong, nonatomic) NSString *artist;
@property (strong, nonatomic) NSString *duration;
@property (strong, nonatomic) NSURL *imageURL;
@property (strong, nonatomic) NSURL *previewURL;
@property (strong, nonatomic) NSString *status;
@property (strong, nonatomic) UIColor *statusColor;
@property (strong, nonatomic) NSString *title;
@property (strong, nonatomic) NSString *type;
@property (strong, nonatomic) NSString *iTunesTitle;
@property (strong, nonatomic) NSURL *iTunesURL;
@property (nonatomic, getter = isAudioPlaying) BOOL audioPlaying;
@property (nonatomic, getter = isPlayAudio) BOOL playAudio;

- (instancetype)initWithPlayoutItem:(AIMPlayoutItem *)item;

// Gets the view models of the current playlist. 
+ (void)getNowPlayingViewModelsWithSuccess:(void (^)(NSArray<AIMNowPlayingViewModel *> *viewModels))success failure:(void (^)(NSError *error))failure;

@end
