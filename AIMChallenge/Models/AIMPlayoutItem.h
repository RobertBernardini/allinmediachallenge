//
//  AIMPlayoutItem.h
//  AIMChallenge
//
//  Created by Robert Bernardini on 9/18/18.
//  Copyright © 2018 Robert Bernardini. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AIMPlayoutItem : NSObject

@property (strong, nonatomic) NSString *artist;
@property (strong, nonatomic) NSDate *duration;
@property (strong, nonatomic) NSString *imageURL;
@property (strong, nonatomic) NSString *previewURL;
@property (strong, nonatomic) NSString *status;
@property (strong, nonatomic) NSString *subtitle;
@property (strong, nonatomic) NSDate *time;
@property (strong, nonatomic) NSString *title;
@property (strong, nonatomic) NSString *type;
@property (strong, nonatomic) NSString *iTunesTitle;
@property (strong, nonatomic) NSString *iTunesURL;

// Instantiates the entity from the dictionary obtained from the 'playoutItem' tag.
- (instancetype)initWithPlayoutItemDictionary:(NSDictionary *)dic;

// Sets the iTunesTitle and iTuneURL properties from the dictionaries obtained from the 'customField' tags.
- (void)setPropertiesFromCustomFieldDictionary:(NSDictionary *)dic;

@end
