//
//  AIMPlayoutItem.m
//  AIMChallenge
//
//  Created by Robert Bernardini on 9/18/18.
//  Copyright © 2018 Robert Bernardini. All rights reserved.
//

#import "AIMPlayoutItem.h"
#import "NSDictionary+Helper.h"
#import "NSDate+Helper.h"

@implementation AIMPlayoutItem

#pragma mark - Lifecycle

- (instancetype)initWithPlayoutItemDictionary:(NSDictionary *)dic {
    self = [super init];
    if (self) {
        self.artist = [dic notNullForKey:@"artist"];
        self.duration = [NSDate durationDateFromString:[dic notNullForKey:@"duration"]];
        self.imageURL = [dic notNullForKey:@"imageUrl"];
        self.previewURL = [dic notNullForKey:@"previewUrl"];
        self.status = [dic notNullForKey:@"status"];
        self.subtitle = [dic notNullForKey:@"subtitle"];
        self.time = [NSDate dateFromString:[dic notNullForKey:@"time"]];
        self.title = [dic notNullForKey:@"title"];
        self.type = [dic notNullForKey:@"type"];
    }
    return self;
}

#pragma mark - Public

- (void)setPropertiesFromCustomFieldDictionary:(NSDictionary *)dic {
    NSString *name = [dic notNullForKey:@"name"];
    if (!name) {
        return;
    }
    
    if ([name isEqualToString:@"link-buy-track-itunes-desc"]) {
        self.iTunesTitle = [dic notNullForKey:@"value"];
    } else if ([name isEqualToString:@"link-buy-track-itunes-url"]) {
        self.iTunesURL = [dic notNullForKey:@"value"];
    }
}

@end
