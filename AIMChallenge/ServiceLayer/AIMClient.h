//
//  AIMClient.h
//  AIMChallenge
//
//  Created by Robert Bernardini on 9/17/18.
//  Copyright © 2018 Robert Bernardini. All rights reserved.
//

#import <Foundation/Foundation.h>
@class AIMPlayoutItem;

@interface AIMClient : NSObject

// Returns a singleton of the AIMClient.
+ (instancetype)sharedClient;

// Gets the Nova Playlist by calling Nova now playing link and retrieving and parsing the XML list.
// If there is no internet connection or an error occurs the failure block is called with an error object.
- (void)getNowPlayingFromNovaWithSuccess:(void (^)(NSArray<AIMPlayoutItem *> *playoutItems))success failure:(void (^)(NSError *error))failure;

@end
