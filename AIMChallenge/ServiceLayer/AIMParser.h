//
//  AIMParser.h
//  AIMChallenge
//
//  Created by Robert Bernardini on 9/14/18.
//  Copyright © 2018 Robert Bernardini. All rights reserved.
//

#import <Foundation/Foundation.h>
@class AIMPlayoutItem;

@interface AIMParser : NSObject <NSXMLParserDelegate>

// Mutable array containing AIMPlayoutItem objects: the audio track objects parsed from
// the returned XML Nova playlist.
@property (strong, nonatomic) NSMutableArray<AIMPlayoutItem *> *playoutItems;

@end
