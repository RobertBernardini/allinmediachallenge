//
//  AIMClient.m
//  AIMChallenge
//
//  Created by Robert Bernardini on 9/17/18.
//  Copyright © 2018 Robert Bernardini. All rights reserved.
//

#import "AIMClient.h"
#import "AIMParser.h"
#import "Reachability.h"
#import "NSError+Helper.h"

static NSString *const novaPlaylistURL = @"http://fred.aimapi.io/services/nowplaying/nova/nova100/onair.xml";


@implementation AIMClient

#pragma mark - Lifecycle

+ (instancetype)sharedClient {
    static id _sharedClient = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedClient = [AIMClient new];
    });
    return _sharedClient;
}

#pragma mark - Public

- (void)getNowPlayingFromNovaWithSuccess:(void (^)(NSArray<AIMPlayoutItem *> *playoutItems))success failure:(void (^)(NSError *error))failure {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        NSURL *url = [NSURL URLWithString:novaPlaylistURL];
        NSXMLParser *xmlParser = [[NSXMLParser alloc] initWithContentsOfURL:url];
        AIMParser *aimParser = [AIMParser new];
        xmlParser.delegate = aimParser;
        [xmlParser parse];
        if (aimParser.playoutItems.count > 0) {
            dispatch_async(dispatch_get_main_queue(), ^{
                success([aimParser.playoutItems copy]);
            });
            return;
        }
        
        if ([Reachability reachabilityForInternetConnection].currentReachabilityStatus == NotReachable) {
            NSError *connectionError = [NSError errorWithCode:NSURLErrorNotConnectedToInternet message:@"Please check that there is an internet connection."];
            dispatch_async(dispatch_get_main_queue(), ^{
                failure(connectionError);
            });
            return;
        }
        
        NSError *downloadError = [NSError errorWithCode:500 message:@"An error occurred obtaining the playlist."];
        dispatch_async(dispatch_get_main_queue(), ^{
            failure(downloadError);
        });
    });
}

@end
