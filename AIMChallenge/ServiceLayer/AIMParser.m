//
//  AIMParser.m
//  AIMChallenge
//
//  Created by Robert Bernardini on 9/14/18.
//  Copyright © 2018 Robert Bernardini. All rights reserved.
//

#import "AIMParser.h"
#import "AIMPlayoutItem.h"


@interface AIMParser()

@property (strong, nonatomic) AIMPlayoutItem *playoutItem;

@end


@implementation AIMParser

#pragma mark - Lifecycle

- (instancetype)init {
    self = [super init];
    if (self) {
        self.playoutItems = [NSMutableArray new];
    }
    return self;
}

#pragma mark - NSXMLParserDelegate

- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary<NSString *,NSString *> *)attributeDict {
    if ([elementName isEqualToString:@"playoutItem"]) {
        self.playoutItem = [[AIMPlayoutItem alloc] initWithPlayoutItemDictionary:attributeDict];
        
    } else if ([elementName isEqualToString:@"customField"]) {
        [self.playoutItem setPropertiesFromCustomFieldDictionary:attributeDict];
    }
}

- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
    if ([elementName isEqualToString:@"playoutItem"]) {
        [self.playoutItems addObject:self.playoutItem];
        self.playoutItem = nil;
    }
}

@end
