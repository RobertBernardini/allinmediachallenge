//
//  AIMNowPlayingTableViewCell.h
//  AIMChallenge
//
//  Created by Robert Bernardini on 9/18/18.
//  Copyright © 2018 Robert Bernardini. All rights reserved.
//

#import <UIKit/UIKit.h>
@class AIMNowPlayingTableViewCell, AIMNowPlayingViewModel;

@protocol AIMNowPlayingTableViewCellDelegate <NSObject>

@required
// Delegate method that indicates when the play audio preview option has been selected.
- (void)nowPlayingTableViewCell:(AIMNowPlayingTableViewCell *)cell didSelectPlayPreviewForViewModel:(AIMNowPlayingViewModel *)viewModel;

// Delegate method that indicates when the open iTunes option has been selected.
- (void)nowPlayingTableViewCell:(AIMNowPlayingTableViewCell *)cell didSelectOpeniTunesForViewModel:(AIMNowPlayingViewModel *)viewModel;

@end

@interface AIMNowPlayingTableViewCell : UITableViewCell

@property (weak, nonatomic) id<AIMNowPlayingTableViewCellDelegate> delegate;

// Updates the cell for the view model.
- (void)updateCellForViewModel:(AIMNowPlayingViewModel *)viewModel;

@end
