//
//  AIMNowPlayingTableViewCell.m
//  AIMChallenge
//
//  Created by Robert Bernardini on 9/18/18.
//  Copyright © 2018 Robert Bernardini. All rights reserved.
//

#import <SDWebImage/UIImageView+WebCache.h>
#import "AIMNowPlayingTableViewCell.h"
#import "AIMNowPlayingViewModel.h"
#import "AIMNowPlayingViewController.h"

@interface AIMNowPlayingTableViewCell()

@property (weak, nonatomic) IBOutlet UIImageView *songImageView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *artistLabel;
@property (weak, nonatomic) IBOutlet UILabel *durationLabel;
@property (weak, nonatomic) IBOutlet UILabel *statusLabel;
@property (weak, nonatomic) IBOutlet UIButton *previewButton;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *downloadingAudioActivityIndicator;
@property (weak, nonatomic) IBOutlet UIButton *iTunesButton;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *previewButtonContainerViewWidth;

@property (strong, nonatomic) AIMNowPlayingViewModel *viewModel;

@end

@implementation AIMNowPlayingTableViewCell

#pragma mark - Lifecycle

- (void)awakeFromNib {
    [super awakeFromNib];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didStopPlayingAudio:) name:kDidStopPlayingAudioNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didStartDownloadingAudio:) name:kDidStartDownloadingAudioNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didStopDownloadingAudio:) name:kDidStopDownloadingAudioNotification object:nil];
    CGFloat iTunesButtonFontSize = [UIScreen mainScreen].fixedCoordinateSpace.bounds.size.height == 568 ? 9 : 11;
    self.iTunesButton.titleLabel.font = [UIFont systemFontOfSize:iTunesButtonFontSize];
    self.downloadingAudioActivityIndicator.hidden = YES;
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - IBActions

- (IBAction)didClickPreviewButton:(UIButton *)sender {
    sender.selected = !sender.isSelected;
    [self.delegate nowPlayingTableViewCell:self didSelectPlayPreviewForViewModel:self.viewModel];
}

- (IBAction)didClickiTunesButton:(UIButton *)sender {
    [self.delegate nowPlayingTableViewCell:self didSelectOpeniTunesForViewModel:self.viewModel];
}

#pragma mark - Public

- (void)updateCellForViewModel:(AIMNowPlayingViewModel *)viewModel {
    self.viewModel = viewModel;
    [self.songImageView sd_setImageWithURL:viewModel.imageURL placeholderImage:[UIImage imageNamed:@"image-placeholder"]];
    self.titleLabel.text = viewModel.title;
    self.artistLabel.text = viewModel.artist;
    self.durationLabel.text = viewModel.duration;
    self.statusLabel.text = viewModel.status;
    self.statusLabel.textColor = viewModel.statusColor;
    self.previewButtonContainerViewWidth.constant = !self.viewModel.previewURL ? 0 : 50;
    self.iTunesButton.hidden = !self.viewModel.iTunesURL;
    [self.iTunesButton setTitle:viewModel.iTunesTitle forState:UIControlStateNormal];
    self.previewButton.selected = viewModel.isAudioPlaying;
}

#pragma mark - NSNotifications

- (void)didStartDownloadingAudio:(NSNotification *)notification {
    AIMNowPlayingViewModel *viewModel = notification.object;
    if ([viewModel isEqual:self.viewModel]) {
        self.downloadingAudioActivityIndicator.hidden = NO;
        [self.downloadingAudioActivityIndicator startAnimating];
    }
}

- (void)didStopDownloadingAudio:(NSNotification *)notification {
    AIMNowPlayingViewModel *viewModel = notification.object;
    if ([viewModel isEqual:self.viewModel]) {
        self.downloadingAudioActivityIndicator.hidden = YES;
        [self.downloadingAudioActivityIndicator stopAnimating];
    }
}

- (void)didStopPlayingAudio:(NSNotification *)notification {
    AIMNowPlayingViewModel *viewModel = notification.object;
    if ([viewModel isEqual:self.viewModel]) {
        self.previewButton.selected = viewModel.isAudioPlaying;
    }
}

@end
