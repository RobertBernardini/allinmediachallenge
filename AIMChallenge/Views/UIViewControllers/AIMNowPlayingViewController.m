//
//  AIMNowPlayingViewController.m
//  AIMChallenge
//
//  Created by Robert Bernardini on 9/18/18.
//  Copyright © 2018 Robert Bernardini. All rights reserved.
//

@import AVFoundation;
#import "AIMNowPlayingViewController.h"
#import "AIMNowPlayingTableViewCell.h"
#import "AIMNowPlayingViewModel.h"
#import "UIViewController+Alert.h"

NSString *const kDidStartDownloadingAudioNotification = @"DidStartDownloadingAudioNotification";
NSString *const kDidStopDownloadingAudioNotification = @"DidStopDownloadingAudioNotification";
NSString *const kDidStopPlayingAudioNotification = @"DidStopPlayingAudioNotification";

@interface AIMNowPlayingViewController ()<UITableViewDataSource, AIMNowPlayingTableViewCellDelegate, AVAudioPlayerDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *loadingListActivityIndicator;

@property (strong, nonatomic) UIRefreshControl *refreshControl;
@property (strong, nonatomic) AVAudioPlayer *audioPlayer;
@property (strong, nonatomic) NSArray<AIMNowPlayingViewModel *> *nowPlayingViewModels;
@property (strong, nonatomic) AIMNowPlayingViewModel *selectedViewModel;
@property (nonatomic, getter = isUpdatingPlaylist) BOOL updatingPlaylist;
@property (nonatomic, getter = isDownloadingAudio) BOOL downloadingAudio;

@end

@implementation AIMNowPlayingViewController

#pragma mark - Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    self.tableView.refreshControl = self.refreshControl;
    [self loadPlaylist];
}

#pragma mark - Custom Accessors

- (UIRefreshControl *)refreshControl {
    if (!_refreshControl) {
        _refreshControl = [UIRefreshControl new];
        UIColor *refreshColor = [UIColor colorWithRed:193.0/255 green:235.0/255 blue:255.0/255 alpha:1.0];
        _refreshControl.tintColor = refreshColor;
        NSDictionary *attributes = @{NSForegroundColorAttributeName: refreshColor};
        _refreshControl.attributedTitle = [[NSAttributedString alloc] initWithString:@"Updating Playlist..." attributes:attributes];
        [_refreshControl addTarget:self action:@selector(refreshPlaylist) forControlEvents:UIControlEventValueChanged];
    }
    return _refreshControl;
}

#pragma mark - Private

// Obtains view model playlist array and updates table.
- (void)updatePlaylistWithCompletion:(void (^)(void))completion {
    if (self.isUpdatingPlaylist) {
        return;
    }
    
    [self.audioPlayer stop];
    [self audioPlayerDidFinishPlaying:self.audioPlayer successfully:YES];
    
    self.updatingPlaylist = YES;
    [AIMNowPlayingViewModel getNowPlayingViewModelsWithSuccess:^(NSArray<AIMNowPlayingViewModel *> *viewModels) {
        self.updatingPlaylist = NO;
        self.nowPlayingViewModels = viewModels;
        [self.tableView reloadData];
        completion();
        
    } failure:^(NSError *error) {
        self.updatingPlaylist = NO;
        [self showAlertWithTitle:@"Error" message:error.localizedDescription completion:^{
            completion();
        }];
    }];
}

// Called to load the playlist when the app starts.
- (void)loadPlaylist {
    self.tableView.alpha = 0;
    [self.loadingListActivityIndicator startAnimating];
    [self updatePlaylistWithCompletion:^{
        [UIView animateWithDuration:0.3 animations:^{
            self.tableView.alpha = 1;
            self.loadingListActivityIndicator.alpha = 0;
        } completion:^(BOOL finished) {
            [self.loadingListActivityIndicator stopAnimating];
        }];
    }];
}

// Called to update the playlist when refreshing the table.
- (void)refreshPlaylist {
    if (self.isDownloadingAudio) {
        [self.refreshControl endRefreshing];
        return;
    }
    
    [self updatePlaylistWithCompletion:^{
        [self.refreshControl endRefreshing];
    }];
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.nowPlayingViewModels.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    AIMNowPlayingTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"NowPlayingCell" forIndexPath:indexPath];
    AIMNowPlayingViewModel *viewModel = self.nowPlayingViewModels[indexPath.row];
    cell.delegate = self;
    [cell updateCellForViewModel:viewModel];
    return cell;
}

#pragma mark - NowPlayingTableViewCellDelegate

// Downloads and plays the the selected audio track when the delegate method is called updating the cell through a notification.
// If the selected audio track is already playing it is stopped and the cell updated through a notification.
// If a different audio track is already being played it is stopped and the cell updated through a notification.
- (void)nowPlayingTableViewCell:(AIMNowPlayingTableViewCell *)cell didSelectPlayPreviewForViewModel:(AIMNowPlayingViewModel *)viewModel {
    if ([viewModel isEqual:self.selectedViewModel] && viewModel.audioPlaying == YES) {
        [self.audioPlayer stop];
        [self audioPlayerDidFinishPlaying:self.audioPlayer successfully:YES];
        return;
    }
    
    if (self.selectedViewModel.audioPlaying == YES) {
        [self.audioPlayer stop];
        [self audioPlayerDidFinishPlaying:self.audioPlayer successfully:YES];
    }
    
    if (self.isDownloadingAudio) {
        [[NSNotificationCenter defaultCenter] postNotificationName:kDidStopPlayingAudioNotification object:viewModel];
        return;
    }
    
    [[NSNotificationCenter defaultCenter] postNotificationName:kDidStartDownloadingAudioNotification object:viewModel];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        self.downloadingAudio = YES;
        NSError *error;
        NSData *audioData = [NSData dataWithContentsOfURL:viewModel.previewURL];
        self.audioPlayer = [[AVAudioPlayer alloc] initWithData:audioData error:&error];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [[NSNotificationCenter defaultCenter] postNotificationName:kDidStopDownloadingAudioNotification object:viewModel];
            self.downloadingAudio = NO;

            if (error) {
                self.audioPlayer = nil;
                [[NSNotificationCenter defaultCenter] postNotificationName:kDidStopPlayingAudioNotification object:viewModel];
                [self showAlertWithTitle:@"Error" message:error.localizedDescription completion:nil];
                return;
            }
            
            self.selectedViewModel = viewModel;
            self.selectedViewModel.audioPlaying = YES;
            self.audioPlayer.delegate = self;
            [self.audioPlayer play];
        });
    });
}

// Opens the iTunes link for the selected audio track when the delegate method is called.
- (void)nowPlayingTableViewCell:(AIMNowPlayingTableViewCell *)cell didSelectOpeniTunesForViewModel:(AIMNowPlayingViewModel *)viewModel {
    [[UIApplication sharedApplication] openURL:viewModel.iTunesURL options:@{} completionHandler:^(BOOL success) {
        if (!success) {
            [self showAlertWithTitle:@"Error" message:@"Could not open iTunes for the selected track" completion:nil];
        }
    }];
}

#pragma mark - AVAudioPlayerDelegate

- (void)audioPlayerDidFinishPlaying:(AVAudioPlayer *)player successfully:(BOOL)flag {
    player = nil;
    self.selectedViewModel.audioPlaying = NO;
    [[NSNotificationCenter defaultCenter] postNotificationName:kDidStopPlayingAudioNotification object:self.selectedViewModel];
}

@end
