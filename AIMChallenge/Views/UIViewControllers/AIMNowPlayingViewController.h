//
//  AIMNowPlayingViewController.h
//  AIMChallenge
//
//  Created by Robert Bernardini on 9/18/18.
//  Copyright © 2018 Robert Bernardini. All rights reserved.
//
// View controller that displays the Playlist.

#import <UIKit/UIKit.h>

// Notification name constants broadcast when preview audio starts or stops downloading and stops playing.
extern NSString *const kDidStartDownloadingAudioNotification;
extern NSString *const kDidStopDownloadingAudioNotification;
extern NSString *const kDidStopPlayingAudioNotification;

@interface AIMNowPlayingViewController : UIViewController

@end
