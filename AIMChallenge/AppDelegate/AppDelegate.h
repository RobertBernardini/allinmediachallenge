//
//  AppDelegate.h
//  AIMChallenge
//
//  Created by Robert Bernardini on 9/13/18.
//  Copyright © 2018 Robert Bernardini. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

